-- Verify sc_vlci_userdb:r_code/functions/f.fn_aud_fec_ins on pg

BEGIN;

    SELECT has_function_privilege('public.fn_aud_fec_ins()', 'execute');

ROLLBACK;
