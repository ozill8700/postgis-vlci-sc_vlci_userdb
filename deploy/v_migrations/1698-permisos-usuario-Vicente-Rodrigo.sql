-- Deploy sc_vlci_userdb:v_migrations/1698-permisos-usuario-Vicente-Rodrigo to pg

BEGIN;

INSERT INTO sc_vlci_userdb.public.users (username,"password",enabled,description) VALUES
	('U19535','Password1',1,'Vicente Rodrigo Ingresa');

INSERT INTO sc_vlci_userdb.public.users (username,"password",enabled,description) VALUES
	('u19535','Password1',1,'Vicente Rodrigo Ingresa');

INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) VALUES
	('U19535','AccUnificadoEconomicoCiudad');

INSERT INTO sc_vlci_userdb.public.granted_authorities (username,authority) VALUES
	('u19535','AccUnificadoEconomicoCiudad');

UPDATE sc_vlci_userdb.public.granted_authorities
SET authority='AccUnificadoEconomicoCiudad' where username='LVRI';

UPDATE sc_vlci_userdb.public.granted_authorities
SET authority='AccUnificadoEconomicoCiudad' where username='lvri';

COMMIT;
