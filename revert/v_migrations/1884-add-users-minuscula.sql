-- Revert sc_vlci_userdb:v_migrations/1884-add-users-minuscula from pg

BEGIN;

DELETE FROM public.granted_authorities where username in ('u18761', 'u301362 ');
DELETE FROM public.users where username in ('u18761', 'u301362 ');

COMMIT;
