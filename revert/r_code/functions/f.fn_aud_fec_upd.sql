-- Revert sc_vlci_userdb:r_code/functions/f.fn_aud_fec_upd from pg

BEGIN;

    DROP FUNCTION IF EXISTS public.fn_aud_fec_upd() CASCADE;

COMMIT;
