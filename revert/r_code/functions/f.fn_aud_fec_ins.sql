-- Revert sc_vlci_userdb:r_code/functions/f.fn_aud_fec_ins from pg

BEGIN;

    DROP FUNCTION IF EXISTS public.fn_aud_fec_ins() CASCADE;

COMMIT;
